﻿using System.Collections.Generic;
using System.Text;

namespace InterviewAssessment
{
    public class Scenarios
    {
        /// <summary>
        /// Implement this method.
        /// </summary>
        public string Scenario1(string str) {
            if (string.IsNullOrEmpty(str))
            {
                return "";
            }
            else
            {
                //Method 1: using built-in functions
                //char[] charArray = str.ToCharArray();
                //System.Array.Reverse(charArray);
                //return new string(charArray);

                //Method 2: reversal implementation 
                int length = str.Length;
                if (length == 1)
                {
                    return str;
                }
                else
                {
                    char[] charArray = str.ToCharArray();
                    int first = 0;
                    int last = length - 1;
                    while (first < last)
                    {
                        //could also use XOR for reversal without additional variables
                        char temp = charArray[first];
                        charArray[first] = charArray[last];
                        charArray[last] = temp;
                        first++;
                        last--;
                    }
                    return new string(charArray);
                }
            }
        }
        

        /// <summary>
        /// Fix this method.
        /// </summary>
        public int Scenario2(int @base, int exponent)
        {
            int n = 1;

            for (int i = 0; i < exponent; i++)
            {
                n *= @base;
            }

            return n;
        }

        /// <summary>
        /// DO NOT MODIFY
        /// </summary>
        public string Scenario3<T>(T obj) => typeof(T).Name;

        /// <summary>
        /// DO NOT MODIFY
        /// </summary>
        public string Scenario3(string str) => str;

        /// <summary>
        /// Implement this method.
        /// </summary>
        public string Scenario4(Node node){
            if (node == null)
            {
                return "";
            }
            else if(node.GetType().Equals(typeof(Node)))
            {
                return node.Text;
            }
            else
            {
                StringBuilder tree = new StringBuilder();
                tree.Append(node.Text);
                foreach(Node child in ((Tree)node).Children){
                    tree.Append('-');
                    tree.Append(Scenario4(child));
                }
                return tree.ToString();
            }                          
        }
    }

    public class Node
    {
        public Node(string text)
        {
            Text = text;
        }

        public string Text { get; set; }
    }

    public class Tree : Node
    {
        public Tree(string text, params Node[] children) : base(text)
        {
            Children = children;
        }

        public IEnumerable<Node> Children { get; set; }
    }
}
